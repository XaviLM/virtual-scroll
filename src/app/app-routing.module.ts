import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Error404Component } from './components/error404/error404.component';
import { AppComponent } from './app.component';
import { MainComponent } from './components/main/main.component';
import { VirtualComponent } from './components/virtual/virtual.component';
import { DragComponent } from './components/drag/drag.component';
import { PaisesComponent } from './components/paises/paises.component';

const routes: Routes = [
  { path: 'main', component: MainComponent },
  { path: '', component: VirtualComponent },
  { path: 'virtual', component: VirtualComponent },
  { path: 'drag', component: DragComponent },
  { path: 'paises', component: PaisesComponent },
  { path: '**', component: Error404Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
